---
title: Update version to Aledo
date: '2021-02-10T12:07:35.287Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2021-02-10T12:43:10.610Z'
---
We are updating the version. It will take a few minutes

<!--- language code: en -->
