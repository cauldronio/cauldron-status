---
title: Update Cauldron to a new release
date: '2021-05-26T07:32:33.061Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2021-05-26T11:26:13.683Z'
---
We are updating Cauldron to a newer version, it will take some minutes

<!--- language code: en -->
