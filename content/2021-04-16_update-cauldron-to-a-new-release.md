---
title: Update Cauldron to a new release
date: '2021-04-16T07:19:17.781Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2021-04-16T08:14:25.959Z'
---
We are updating Cauldron to a newer version, it will take some minutes.

<!--- language code: en -->
