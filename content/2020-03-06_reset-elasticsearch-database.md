---
title: Reset Elasticsearch Database
date: '2020-03-06T16:08:10.908Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
scheduled: '2020-04-01T13:00:00.000Z'
duration: '120'
---
Dear users, the Cauldron.io team is hard at work to improve data consistency, increase stability, and implement new data protection laws. We regret having to reset our database in the process. After April 1, you will need to refresh your data for completely new data to be collected. Our sincerest apologies but we are confident that you will enjoy the upcoming improvements.

<!--- language code: en -->
