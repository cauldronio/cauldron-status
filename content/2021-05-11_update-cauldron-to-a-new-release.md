---
title: Update Cauldron to a new release
date: '2021-05-11T09:51:50.908Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2021-05-11T11:54:46.407Z'
---
Updating Cauldron to a new release, it will take some minutes.

<!--- language code: en -->
