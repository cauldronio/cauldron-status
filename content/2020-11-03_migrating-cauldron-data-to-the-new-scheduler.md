---
title: Migrating Cauldron data to the new scheduler
date: '2020-11-03T09:58:23.757Z'
severity: major-outage
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-11-03T12:58:23.757Z'
---
We are performing a migration of all the Cauldron data to a new database with new models. This update will take some hours.

<!--- language code: en -->
