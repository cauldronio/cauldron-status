---
title: Upgrade Cauldron to Quinoa release
date: '2020-08-14T14:34:34.930Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-08-15T08:00:00.000Z'
---
We are updating Cauldron to a newer version. It will take a few minutes

<!--- language code: en -->
