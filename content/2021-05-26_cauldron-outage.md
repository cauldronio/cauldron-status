---
title: Cauldron Maintenance
date: '2021-05-26T08:53:00.000Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2021-05-26T11:27:45.709Z'
---
Cauldron Team is fixing some problems with the database. We will be operational soon.

<!--- language code: en -->
