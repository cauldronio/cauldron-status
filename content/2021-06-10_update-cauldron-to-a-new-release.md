---
title: Update Cauldron to a new release
date: '2021-06-10T09:21:40.934Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2021-06-10T10:21:40.934Z'
---
We are updating Cauldron to a newer version, we have to migrate some data. It will take about 1 hour.

<!--- language code: en -->
