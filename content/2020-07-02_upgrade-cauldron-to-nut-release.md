---
title: Upgrade Cauldron to Nut release
date: '2020-07-02T12:59:04.057Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-07-02T13:13:32.798Z'
---
We are updating Cauldron to a newer version. It will take a few hours.

<!--- language code: en -->
