---
title: Upgrade Cauldron to wakame
date: '2020-11-25T09:34:45.921Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-11-25T11:30:00.000Z'
---
We are updating Cauldron to a newer version. It will take a few hours. Sorry for the inconvenience.

<!--- language code: en -->
