---
title: Upgrade Cauldron to Yogurt
date: '2021-01-12T15:37:28.558Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2021-01-12T16:17:12.979Z'
---
We are updating Cauldron to a newer version. It will take some hours.

<!--- language code: en -->
