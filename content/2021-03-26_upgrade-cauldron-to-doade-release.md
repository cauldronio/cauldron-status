---
title: Upgrade Cauldron to Doade release
date: '2021-03-26T16:03:03.991Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2021-03-26T16:53:52.361Z'
---
We are updating Cauldron to a newer version. It will take a few hours.

<!--- language code: en -->
