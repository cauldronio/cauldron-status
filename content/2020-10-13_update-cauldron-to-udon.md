---
title: Update Cauldron to Udon
date: '2020-10-13T14:34:02.945Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-10-13T14:39:36.437Z'
---
We are updating Cauldron to the latest version, it will take a few minutes

<!--- language code: en -->
