---
title: Update Cauldron to Honey
date: '2020-04-01T14:00:00.000Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
---

We are updating Cauldron to the latest release (Honey)
