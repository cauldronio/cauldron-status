---
title: Update Cauldron to Ketchup
date: '2020-05-20T15:48:02.567Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-05-20T16:02:38.546Z'
---
We are updating Cauldron to a newer version. It will take a few hours.

<!--- language code: en -->
