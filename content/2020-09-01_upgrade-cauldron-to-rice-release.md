---
title: 'Upgrade Cauldron to Rice release '
date: '2020-09-01T08:26:01.917Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-09-01T09:06:59.388Z'
---
We are updating Cauldron to a newer version. It will take a few minutes

<!--- language code: en -->
