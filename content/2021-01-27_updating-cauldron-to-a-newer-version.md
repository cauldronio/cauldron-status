---
title: Updating Cauldron to a newer version
date: '2021-01-27T14:27:24.934Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2021-01-27T14:54:30.877Z'
---
Cauldron is being updated, it will take some minutes.

<!--- language code: en -->
