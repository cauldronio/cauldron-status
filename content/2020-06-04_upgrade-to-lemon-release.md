---
title: Upgrade Cauldron to Lemon release
date: '2020-06-04T11:21:01.634Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-06-04T11:45:36.987Z'
---
We are updating Cauldron to a newer version. We will be back in less than 1 hour.

<!--- language code: en -->
