---
title: Update to Salt release
date: '2020-09-16T15:11:37.536Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
modified: '2020-09-16T15:16:46.100Z'
---
We are updating to a new version, it will take a few minutes

<!--- language code: en -->
