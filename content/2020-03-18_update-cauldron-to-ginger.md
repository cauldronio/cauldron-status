---
title: Update Cauldron to Ginger
date: '2020-03-18T09:00:00.000Z'
severity: under-maintenance
affectedsystems:
  - web
  - kibana
  - elasticsearch
  - workers
resolved: true
---

We are updating Cauldron to the latest release (Ginger)
